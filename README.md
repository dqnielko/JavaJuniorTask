# JavaJuniorTask
                                    Create REST Web-Service using Spring Boot

Acceptance criteria:

Domain:

You can choose any domain that you like. Example: Video Games

There should be at least 2 entities (tables with relations) – Example:

Genre->Game -> Store etc

                                    Restful web service for CRUD operation:

User should be able to Create/Read/Update/Delete entities via HTTP calls (Preferably REST API)

Web Service should be secured via Basic AUTH (users can be stored in memory/file/db)

Additional JWT auth service could be added (More complex task)

                            Scheduled service to parse CSV data sources into DB

Create scheduled service that will scan directory in File System for CSV files with data and parse data into entities and save into DB

Service should use several threads for processing in case of several files found (to learn multithreading a bit)

Result (success or errors) should be saved somewhere (log files, DB, etc) 