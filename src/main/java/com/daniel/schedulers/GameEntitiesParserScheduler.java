package com.daniel.schedulers;

import com.daniel.parser.game.GameEntitiesParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class GameEntitiesParserScheduler {
    private final int schedulerDelay = 5000;
    private final String targetDirectory = "filesToParse";
    private final GameEntitiesParser gameEntitiesParser;

    @Autowired
    public GameEntitiesParserScheduler(GameEntitiesParser gameEntitiesParser){
        this.gameEntitiesParser = gameEntitiesParser;
        this.gameEntitiesParser.setTargetDirectory(targetDirectory);
    }

    @Scheduled(fixedDelay = schedulerDelay)
    public void parseFiles(){
        gameEntitiesParser.scanDirectory();
        gameEntitiesParser.parseFiles();
    }
}
