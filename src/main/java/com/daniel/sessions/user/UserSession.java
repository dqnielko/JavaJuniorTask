package com.daniel.sessions.user;

import lombok.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class UserSession{
    private Long id;
    private String login;
    private String password;
}
