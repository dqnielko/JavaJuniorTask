package com.daniel.entities.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class GameStore {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "store",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Set<Game> games = new HashSet<>();
}
