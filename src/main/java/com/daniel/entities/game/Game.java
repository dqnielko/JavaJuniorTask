package com.daniel.entities.game;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Game {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="genre_id",referencedColumnName = "id")
    private GameGenre genre;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="gameStore_id", referencedColumnName = "id")
    private GameStore store;
}