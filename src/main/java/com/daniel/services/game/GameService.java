package com.daniel.services.game;

import com.daniel.entities.game.Game;
import com.daniel.entities.game.GameGenre;
import com.daniel.entities.game.GameStore;
import com.daniel.repositories.game.GameGenreRepository;
import com.daniel.repositories.game.GameRepository;
import com.daniel.repositories.game.GameStoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class GameService {
    private final GameRepository gameRepository;
    private final GameGenreRepository gameGenreRepository;
    private final GameStoreRepository gameStoreRepository;

    @Autowired
    public GameService(GameRepository gameRepository,
                       GameGenreRepository gameGenreRepository,
                       GameStoreRepository gameStoreRepository) {
        this.gameRepository = gameRepository;
        this.gameGenreRepository = gameGenreRepository;
        this.gameStoreRepository = gameStoreRepository;
    }
    public boolean saveGame(String gameName, String genreName, String storeName){
        boolean result = false;
        Game game = gameRepository.findByName(gameName);
        if(game==null){
            GameGenre gameGenre = gameGenreRepository.findByName(genreName);
            GameStore gameStore = gameStoreRepository.findByName(storeName);
            if(gameGenre!=null&&gameStore!=null){
                game = Game.builder().name(gameName).genre(gameGenre).store(gameStore).build();
                gameRepository.save(game);
                result = true;
            }
        }
        return result;
    }
    public boolean saveGameGenre(String genreName){
        GameGenre gameGenre = gameGenreRepository.findByName(genreName);
        boolean result = false;
        if(gameGenre==null){
            gameGenre = GameGenre.builder().name(genreName).build();
            gameGenreRepository.save(gameGenre);
            result = true;
        }
        return result;
    }
    public boolean saveGameStore(String storeName) {
        GameStore gameStore = gameStoreRepository.findByName(storeName);
        boolean result = false;
        if (gameStore == null) {
            gameStore = GameStore.builder().name(storeName).build();
            gameStoreRepository.save(gameStore);
            result = true;
        }
        return result;
    }

    public Game findGameByName(String name){
        return gameRepository.findByName(name);
    }

    public List<Game> findGamesByGenre(String genreName){
        List<Game> gameList = new ArrayList<>();
        GameGenre gameGenre = gameGenreRepository.findByName(genreName);
        if(gameGenre!=null){
            gameList = gameRepository.findByGenreId(gameGenre.getId());
        }
        return gameList;
    }

    public List<Game> findGamesByStore(String storeName){
        List<Game> gameList = new ArrayList<>();
        GameStore gameStore = gameStoreRepository.findByName(storeName);
        if(gameStore!=null){
            gameList = gameRepository.findByStoreId(gameStore.getId());
        }
        return gameList;
    }

    public boolean updateGameName(Long id, String newName){
        boolean result = false;
        Game game = gameRepository.findById(id).orElse(null);
        if(game!=null){
            game.setName(newName);
            gameRepository.save(game);
        }
        return result;
    }

    public boolean updateGenreForGame(Long gameId, Long genreId){
        boolean result = false;
        Game game = gameRepository.findById(gameId).orElse(null);
        GameGenre gameGenre = gameGenreRepository.findById(genreId).orElse(null);
        if(game!=null&&gameGenre!=null){
            game.setGenre(gameGenre);
            gameRepository.save(game);
        }
        return result;
    }

    public boolean updateStoreForGame(Long gameId, Long storeId){
        boolean result = false;
        Game game = gameRepository.findById(gameId).orElse(null);
        GameStore gameStore = gameStoreRepository.findById(storeId).orElse(null);
        if(game!=null&&gameStore!=null){
            game.setStore(gameStore);
            gameRepository.save(game);
        }
        return result;
    }

    public boolean updateGameGenreName(String name, String newName){
        boolean result = false;
        GameGenre gameGenre = gameGenreRepository.findByName(name);
        if(gameGenre!=null){
            gameGenre.setName(newName);
            gameGenreRepository.save(gameGenre);
        }
        return result;
    }
    public boolean updateGameStoreName(String name, String newName){
        boolean result = false;
        GameStore gameStore = gameStoreRepository.findByName(name);
        if(gameStore!=null){
            gameStore.setName(newName);
            gameStoreRepository.save(gameStore);
        }
        return result;
    }
    public boolean deleteGameByName(String gameName){
        boolean result = false;
        Long numberOfDeletions = gameRepository.deleteByName(gameName);
        if(numberOfDeletions>0){
            result = true;
        }
        return result;
    }
}
