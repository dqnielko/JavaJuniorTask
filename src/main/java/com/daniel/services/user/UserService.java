package com.daniel.services.user;

import com.daniel.entities.user.User;
import com.daniel.repositories.user.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final IUserRepository userRepository;
    @Autowired
    public UserService(IUserRepository userRepository){
        this.userRepository = userRepository;
    }
    public User findUserById(Long id){
        return userRepository.findById(id).orElse(null);
    }
    public User findUserByLogin(String login){
        return userRepository.findUserByLogin(login);
    }
    public User findUserByLoginAndPassword(String login, String password){
        return userRepository.findUserByLoginAndPassword(login,password);
    }
    public User saveUser(User user){
        return userRepository.save(user);
    }
}
