package com.daniel.controllers.game;

import com.daniel.entities.game.Game;
import com.daniel.services.game.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class GameDeletingController {
    private GameService gameService;

    @DeleteMapping("/game/{name}")
    public ResponseEntity<Game> deleteGameByName(@PathVariable String name){
        ResponseEntity<Game> responseEntity = ResponseEntity.notFound().build();
        if(gameService.deleteGameByName(name)){
            responseEntity = ResponseEntity.ok().build();
        }
        return responseEntity;
    }
}
