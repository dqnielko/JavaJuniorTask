package com.daniel.controllers.game;

import com.daniel.services.game.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class GameUpdatingController {
    private final GameService gameService;

    @PutMapping("/game/{id}/{newName}")
    public ResponseEntity<String> updateGameName(@PathVariable Long id,
                                               @PathVariable String newName){
        ResponseEntity<String> responseEntity = ResponseEntity.notFound().build();
        if(gameService.updateGameName(id,newName)){
            responseEntity = ResponseEntity.ok().build();
        }
        return responseEntity;
    }

    @PutMapping("/game/{gameId}/genre/{genreId}")
    public ResponseEntity<String> updateGameGenre(@PathVariable Long gameId,
                                                @PathVariable Long genreId){
        ResponseEntity<String> responseEntity = ResponseEntity.notFound().build();
        if(gameService.updateGenreForGame(gameId,genreId)){
            responseEntity = ResponseEntity.ok().build();
        }
        return  responseEntity;
    }

    @PutMapping("/game/{gameId}/store/{storeId}")
    public ResponseEntity<String> updateGameStore(@PathVariable Long gameId,
                                                @PathVariable Long storeId){
        ResponseEntity<String> responseEntity = ResponseEntity.notFound().build();
        if(gameService.updateStoreForGame(gameId,storeId)){
            responseEntity = ResponseEntity.ok().build();
        }
        return  responseEntity;
    }
}
