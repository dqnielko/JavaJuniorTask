package com.daniel.controllers.game;

import com.daniel.entities.game.Game;
import com.daniel.services.game.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/game")
@RequiredArgsConstructor
public class GameReadingController {
    private final GameService gameService;

    @GetMapping
    ResponseEntity<Game> findGameByName(@RequestParam(name="name") String name){
        Game game = gameService.findGameByName(name);
        ResponseEntity<Game> responseEntity = ResponseEntity.notFound().build();
        if(game!=null){
            responseEntity = ResponseEntity.ok().body(game);
        }
        return responseEntity;
    }
    @GetMapping("/genre")
    ResponseEntity<List<Game>> findGamesByGenre(@RequestParam(name="name") String name){
        ResponseEntity<List<Game>> responseEntity = ResponseEntity.notFound().build();
        List<Game> games = gameService.findGamesByGenre(name);
        if(!games.isEmpty()){
            responseEntity = ResponseEntity.ok(games);
        }
        return responseEntity;
    }

    @GetMapping("/store")
    ResponseEntity<List<Game>> findGamesByStore(@RequestParam(name="name") String name){
        ResponseEntity<List<Game>> responseEntity = ResponseEntity.notFound().build();
        List<Game> games = gameService.findGamesByStore(name);
        if(!games.isEmpty()){
            responseEntity = ResponseEntity.ok(games);
        }
        return responseEntity;
    }
}
