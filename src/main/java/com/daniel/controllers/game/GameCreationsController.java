package com.daniel.controllers.game;
import com.daniel.controllers.dtos.game.GameDTO;
import com.daniel.services.game.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/game")
@RequiredArgsConstructor
public class GameCreationsController {
    private final GameService gameService;

    @PostMapping
    public ResponseEntity<String> addGame(@RequestBody GameDTO gameDTO)
    {
        ResponseEntity<String> responseEntity = ResponseEntity.badRequest().build();
        if(gameService.saveGame(gameDTO.getName(),gameDTO.getGenre(),gameDTO.getStore())){
            responseEntity = ResponseEntity.ok().build();
        }
        return responseEntity;
    }

    @PostMapping("/genre")
    public ResponseEntity<String> addGenre(@RequestBody String name){
        ResponseEntity<String> responseEntity = ResponseEntity.badRequest().build();
        if(gameService.saveGameGenre(name)){
            responseEntity = ResponseEntity.ok().build();
        }
        return responseEntity;
    }

    @PostMapping("/store")
    public ResponseEntity<String> addStore(@RequestBody String name){
        ResponseEntity<String> responseEntity = ResponseEntity.badRequest().build();
        if(gameService.saveGameStore(name)){
            responseEntity = ResponseEntity.ok().build();
        }
        return responseEntity;
    }
}
