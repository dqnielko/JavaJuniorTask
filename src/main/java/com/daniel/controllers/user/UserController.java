package com.daniel.controllers.user;

import com.daniel.controllers.dtos.user.UserDTO;
import com.daniel.entities.user.User;
import com.daniel.services.user.UserService;
import com.daniel.sessions.user.UserSession;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final BCryptPasswordEncoder passwordEncoder;
    private final UserService userService;
    private UserSession userSession;

    @PostMapping(path = "/user/register")
    public ResponseEntity<Long> registerUser(@RequestBody UserDTO userDTO){
        System.out.println(userDTO);
        ResponseEntity<Long> responseEntity = ResponseEntity.badRequest().build();
        User user = userService.findUserByLogin(userDTO.getLogin());
        if(user==null){
            user = User.builder().login(userDTO.getLogin()).
                    password(passwordEncoder.encode(userDTO.getPassword())).
                    build();
            user = userService.saveUser(user);
            userSession = UserSession.builder()
                            .id(user.getId())
                            .login(user.getLogin())
                            .password(user.getPassword())
                            .build();
            responseEntity = ResponseEntity.ok(userSession.getId());
        }
        return responseEntity;
    }

    @PostMapping(path="user/login")
    public ResponseEntity<Long> loginUser(@RequestBody UserDTO userDTO) {
        User user = userService.findUserByLoginAndPassword(userDTO.getLogin(),userDTO.getPassword());
        ResponseEntity<Long> responseEntity = ResponseEntity.notFound().build();
        if(user!=null&&passwordEncoder.matches(userDTO.getPassword(),user.getPassword())){
            userSession = UserSession.builder()
                    .id(user.getId())
                    .login(user.getLogin())
                    .password(user.getPassword())
                    .build();
            responseEntity = ResponseEntity.ok(userSession.getId());
        }
        return responseEntity;
    }
}
