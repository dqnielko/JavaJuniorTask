package com.daniel.repositories.game;

import com.daniel.entities.game.GameStore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameStoreRepository extends JpaRepository<GameStore,Long> {
    GameStore findByName(String name);
}
