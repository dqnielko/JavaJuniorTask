package com.daniel.repositories.game;

import com.daniel.entities.game.GameGenre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameGenreRepository extends JpaRepository<GameGenre,Long> {
    GameGenre findByName(String name);
}
