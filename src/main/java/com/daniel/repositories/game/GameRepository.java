package com.daniel.repositories.game;

import com.daniel.entities.game.Game;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GameRepository extends JpaRepository<Game,Long> {
    Game findByName(String name);
    List<Game> findByGenreId(Long id);
    List<Game> findByStoreId(Long id);
    Long deleteByName(String name);
}
