package com.daniel.repositories.user;

import com.daniel.entities.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IUserRepository extends JpaRepository<User,Long> {
    User findUserByLogin(String login);
    User findUserByLoginAndPassword(String login, String password);
}
