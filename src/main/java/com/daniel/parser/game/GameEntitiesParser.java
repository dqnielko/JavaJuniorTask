package com.daniel.parser.game;

import com.daniel.repositories.game.GameGenreRepository;
import com.daniel.repositories.game.GameRepository;
import com.daniel.repositories.game.GameStoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


@Component
public class GameEntitiesParser {
    private final GameRepository gameRepository;
    private final GameGenreRepository gameGenreRepository;
    private final GameStoreRepository gameStoreRepository;
    private final String logFilePath = "logfile.txt";
    private final File logFile;
    private String targetDirectory;
    private List<File> files;
    private final String fileExtension = ".txt";
    @Autowired
    public GameEntitiesParser(GameRepository gameRepository,
                              GameGenreRepository gameGenreRepository,
                              GameStoreRepository gameStoreRepository){
        this.gameRepository = gameRepository;
        this.gameGenreRepository = gameGenreRepository;
        this.gameStoreRepository = gameStoreRepository;
        this.logFile = new File(logFilePath);
    }
    public void setTargetDirectory(String targetDirectory){
        this.targetDirectory = targetDirectory;
        this.files = new ArrayList<>();
    }
    public void scanDirectory(){
        File directory = new File(targetDirectory);
        File[]files = directory.listFiles();
        if(files!=null){
            for(File file: files){
                if(file.getName().contains(fileExtension)){
                    if(!this.files.contains(file)){
                        this.files.add(file);
                    }
                }
            }
        }
    }
    public void parseFiles(){
        for(File file: files){
            new Thread(new GameEntityParser(gameRepository,
                    gameGenreRepository,gameStoreRepository,file,logFile)).start();
        }
    }
}
