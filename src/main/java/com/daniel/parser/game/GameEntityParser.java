package com.daniel.parser.game;

import com.daniel.entities.game.Game;
import com.daniel.entities.game.GameGenre;
import com.daniel.entities.game.GameStore;
import com.daniel.repositories.game.GameGenreRepository;
import com.daniel.repositories.game.GameRepository;
import com.daniel.repositories.game.GameStoreRepository;
import java.io.*;
import java.util.Scanner;

public class GameEntityParser implements Runnable{
    private final File file;
    private final File logFile;
    private RandomAccessFile logFileWriter;
    private final GameRepository gameRepository;
    private final GameGenreRepository gameGenreRepository;
    private final GameStoreRepository gameStoreRepository;
    private final int gameDetailsLength = 3;

    public GameEntityParser(GameRepository gameRepository,
                            GameGenreRepository gameGenreRepository,
                            GameStoreRepository gameStoreRepository,
                            File file,
                            File logFile){
        this.gameRepository = gameRepository;
        this.gameGenreRepository = gameGenreRepository;
        this.gameStoreRepository = gameStoreRepository;
        this.file = file;
        this.logFile = logFile;
    }

    private void parseLine(String line){
        String[] gameDetails = line.split(",");
        if(gameDetails.length==gameDetailsLength){
            String gameName = gameDetails[0];
            String genreName = gameDetails[1];
            String storeName = gameDetails[2];
            createGame(gameRepository,gameGenreRepository,gameStoreRepository,
                    gameName,genreName,storeName);
        }
    }

    synchronized private void createGame(GameRepository gameRepository,
                            GameGenreRepository gameGenreRepository,
                            GameStoreRepository gameStoreRepository,
                            String gameName,
                            String genreName,
                            String storeName){
        if(gameRepository.findByName(gameName)==null){
            GameGenre gameGenre = gameGenreRepository.findByName(genreName);
            if(gameGenre==null){
                gameGenre = GameGenre.builder().name(genreName).build();
                gameGenre = gameGenreRepository.save(gameGenre);
            }
            GameStore gameStore = gameStoreRepository.findByName(storeName);
            if(gameStore==null){
                gameStore = GameStore.builder().name(storeName).build();
                gameStore = gameStoreRepository.save(gameStore);
            }
            System.out.println(gameGenre.getName());
            System.out.println(gameStore.getName());
            Game game = Game.builder().name(gameName).genre(gameGenre).store(gameStore).build();
            gameRepository.save(game);
            logInfo("Game "+gameName+" with genre "+genreName+" from store "+storeName+" was" +
                    " saved to DB!");
        }
    }

    private void logInfo(String info){
        if(logFileWriter==null){
            try{
                logFileWriter = new RandomAccessFile(logFile,"rw");
            }catch (FileNotFoundException fileNotFoundException){
                System.out.println(fileNotFoundException.getMessage());
            }
        }
        try{
            logFileWriter.seek(logFile.length());
            logFileWriter.writeBytes(info);
            logFileWriter.writeByte('\n');
        }catch (IOException ioException){
            System.out.println(ioException.getMessage());
        }
    }

    @Override
    public void run() {
        try{
            Scanner reader = new Scanner(file);
            while(reader.hasNextLine()){
                parseLine(reader.nextLine());
            }
        }catch (FileNotFoundException fileNotFoundException){
            System.out.println(fileNotFoundException.getMessage());
        }
    }
}
